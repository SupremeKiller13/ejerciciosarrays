package org.cuatrovientos.java.arrays;

public class Ejercicio01Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 1 de los Ejercicios de Arrays
		
		int[] numeros = new int[10];
		
		for(int i=0; i<numeros.length; i++){
            System.out.println("Numeros: "+i);
        }

	}

}
