package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class Ejercicio02Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 2 de los Arrays
		
		Scanner readFromConsole = new Scanner(System.in);
		String[] vacios =  new String[10];
		
		for(int i=0;i<vacios.length;i++){
            System.out.println("Introduce un nombre: ");
           vacios[i]=readFromConsole.next();
        }
		for (int i = 0; i < vacios.length; i++) {
			System.out.println("Nombre n�mero "+i+": "+vacios[i]);
		}
	}

}
