package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class Ejercicio03Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 3 de los Arrays
		
	/*	Scanner readFromConsole = new Scanner(System.in);
		int [] numeros = new int[10];
		for(int i=0; i<numeros.length; i++){
            System.out.println("Introduce un n�mero: ");
           numeros[i]=readFromConsole.nextInt();
        }
		for (int i = 0; i < numeros.length; i++) {
			for(int j=i+1; j<numeros.length; j++){
			if (numeros[i]==numeros[j]) {
				System.out.println("El n�mero "+numeros[i]+", es igual que el del array");
			}
		}
	}*/
			// Este es el Ejercicio 3 de los Arrays de otra forma
	/*	Scanner readFromConsole = new Scanner(System.in);
			float [] numeros2 = new float[10];
			for(int i=0; i<numeros2.length; i++){
	            System.out.println("Introduce un n�mero: ");
	           numeros2[i]=readFromConsole.nextFloat();
	        }
			for (int i = 0; i < numeros2.length; i++) {
				for(int j=i+1; j<numeros2.length; j++){
				if (numeros2[i]==numeros2[j]) {
					System.out.println("El n�mero "+numeros2[i]+", es igual que el del array");
				}
			}
		} */
			// Problemas de la coma flotante
			
			double a = 0.15 + 0.15; // 0.30
			double b = 0.1 + 0.2; // 0.3
			System.out.println(a==b);
			System.out.println(a>=b);
			
			double epsilon = 0.000000001;
			if (Math.abs(a-b)<epsilon) {
				System.out.println("Los valores los damos por iguales");
			}

			
			// Ejemplo de la funci�n comparaci�n float
			float f1 = 22.30f;
			float f2 = 88.67f;
			
			int retval = Float.compare(f1, f2);
			if (retval > 0) {
				System.out.println("f1 es mejor que f2");
			}else if (retval < 0) {
				System.out.println("f1 es peor que f2");
			}else{
			System.out.println("f1 es igual que f2");
				
			}
}
}