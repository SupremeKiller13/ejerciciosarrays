package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class Ejercicio04Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 4 de Arrays
		
		Scanner readFromConsole = new Scanner(System.in);
		int [] numeros = new int[10];
		for(int i=0; i<numeros.length; i++){
            System.out.println("Introduce un n�mero: ");
           numeros[i]=readFromConsole.nextInt();
		}
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = numeros[i] + 1;
			System.out.println("El n�mero "+numeros[i]);
		}
	}

}
