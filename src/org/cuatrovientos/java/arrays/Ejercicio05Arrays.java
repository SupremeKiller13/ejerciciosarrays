package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class Ejercicio05Arrays {

	public static void main(String[] args) {
		// Este es el ejercicio 5 de los arrays
		
		Scanner readFromConsole = new Scanner(System.in);
		 int i;
	        float numero = 0;
	        float[] numeros = new float[10];
	        float sumaNumeros = 0;
	        for (i = 0; i < 10; i++) {
	        System.out.println("Introduce los n�meros: ");
	        numeros[i] = readFromConsole.nextFloat();
	        }
	        for (i = 0; i < 10; i++) {
	        	sumaNumeros = sumaNumeros + numeros[i];
	        	numero++;
	        }
	        System.out.println("Media de numeros es: " + sumaNumeros / numero);
	}

}
