package org.cuatrovientos.java.arrays;

import java.util.Scanner;

public class Ejercicio07Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 7 de Arrays
		Scanner readFromConsole = new Scanner(System.in);
		int i;
		int[] arrayNumeros = new int[10];
		for (i = 0; i < 10; i++) {
			System.out.print("Introduce n�meros: ");
			arrayNumeros[i] = readFromConsole.nextInt();
		}

		for (int numero : arrayNumeros) {
			int contador = 2;
			boolean primo = true;

			while ((primo) && (contador != numero)) {
				if (numero % contador == 0) {
					primo = false;
					System.out.println(numero + " no es un valor primo");
				}
				contador++;
			}
			if (primo) {
				System.out.println(numero + " es un valor primo");
			}
		}
	}
}
