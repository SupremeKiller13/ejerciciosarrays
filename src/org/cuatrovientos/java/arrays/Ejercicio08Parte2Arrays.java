package org.cuatrovientos.java.arrays;

import java.util.Random;

public class Ejercicio08Parte2Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 8 de Arrays su segunda parte
		
		int num;
		int[][] numeros = new int[5][5];
        Random rnd = new Random();
        
        for (int i=0; i < numeros.length; i++) {
        	  for (int j=0; j < numeros.length; j++) {
        	    num = rnd.nextInt(30);
        	    numeros[i][j]=num;
        	    System.out.println("Valor aleatorio insertado en "+"["+i+"]"+"["+j+"]:"+""+numeros[i][j]);
        	  }
        	}  
        	  for (int i=0; i < numeros.length; i++) {
            	  for (int j=0; j < numeros.length; j++) {
            		  if (numeros[i][j]==15) {
            				System.out.println("El n�mero 15 est� en la posici�n: "+"["+i+"]"+"["+j+"]");
            			}
            	  }
            }
        
	}

}
