package org.cuatrovientos.java.arrays;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio09Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 9 de Arrays
		
		Scanner readFromConsole = new Scanner(System.in);
		int i;
		int num;
		int[] arrayNumeros = new int[10];
		Random rnd = new Random();
		
		for (i = 0; i < 10; i++) {
			System.out.print("Introduce n�meros: ");
			arrayNumeros[i] = readFromConsole.nextInt();
		}
		System.out.println("Valores Generados: ");
		for (i = 0; i < 10; i++) {			
			System.out.println(i+":"+arrayNumeros[i]);
		}
		
		System.out.println("Valores del Array Aleatorios: ");
		int[] arrayindicesAleatorios = new int[10];
		for (i = 0; i < 10; i++) {
			arrayindicesAleatorios[i] = rnd.nextInt(8);
		}
		System.out.println("Valores Generados: ");
		for (i = 0; i < 10; i++) {			
			System.out.println(i+":"+arrayindicesAleatorios[i]);
		}
		
		System.out.println("Intercambia valores: ");
		int[] arrayNumerosAleatorios = new int[10];
		for (i = 0; i < 10; i++) {
			arrayNumerosAleatorios[i] = arrayNumeros[arrayindicesAleatorios[i]];
		}
		System.out.println("Valores Generados: ");
		for (i = 0; i < 10; i++) {			
			System.out.println(i+":"+arrayNumerosAleatorios[i]);
		}
		
	}

}
