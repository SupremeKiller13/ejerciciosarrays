package org.cuatrovientos.java.arrays;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio10Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 10 de Arrays
		
		Scanner readFromConsole = new Scanner(System.in);
		int num;
		int suma = 0, media;
		int[][] numeros = new int[5][5];
        for (int i=0; i < numeros.length; i++) {
        	  for (int j=0; j < numeros.length; j++) {
        	    System.out.print("["+i+"]"+"["+j+"]:"+"Introduce n�meros: ");
        	    numeros[i][j] = readFromConsole.nextInt();
        	    System.out.println();
        	  }
        	} 
        
        for (int i=0; i < numeros.length; i++) {
        	System.out.println("Media de la fila "+i+" es: ");
      	  for (int j=0; j < numeros.length; j++) {
      		  suma = suma + numeros[i][j];
      	  }
      	   media = suma / numeros.length;
        suma = 0;
        System.out.println(media);
      	}  
       
	}

}
