package org.cuatrovientos.java.arrays;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio11Arrays {

	public static void main(String[] args) {
		// Este es el Ejercicio 11 de Arrays
		
		Scanner readFromConsole = new Scanner(System.in);
		int i;
		int numero;
		int buscar = 0;
        int[] numeros = new int[10];
        Random rnd = new Random();
        int contar = 0;
        int borrar;
        
       do { 
        System.out.println("Escribe un n�mero: ");
        System.out.println("1.Generar Array.");
        System.out.println("2.Buscar N�mero.");
        System.out.println("3.Borrar N�mero.");
        System.out.println("4.Salir.");
        System.out.print("Opci�n N�: ");
        numero = readFromConsole.nextInt();

        if (numero == 1) {
        	 for ( i = 0; i < numeros.length; i++) {
             	numeros[i] = rnd.nextInt(10);
             	System.out.println("Este es el valor en "+i+": "+numeros[i]);
        	 }
		} else if (numero == 2) {
			for ( i = 0; i < numeros.length; i++) {
             	numeros[i] = rnd.nextInt(10);
			}
				System.out.println("Introduce el n�mero a buscar: ");
				buscar = readFromConsole.nextInt();
				for ( i = 0; i < numeros.length; i++) {
             	if (numeros[i] == buscar) {
             		contar++;
             	}
             	if (contar > 0) {
					System.out.println("El n�mero "+buscar+" est� en la posici�n: "+i);
             	}else{
					System.out.println("El n�mero"+buscar+"no se encuentra en el array");	
					}
				}
		} else if (numero == 3) {
			for ( i = 0; i < numeros.length; i++) {
             	numeros[i] = rnd.nextInt(10);
			}
			System.out.println("Introduce un n�mero a borrar: ");
			borrar = readFromConsole.nextInt();
			for ( i = 0; i < numeros.length; i++) {
             	if (numeros[i] == borrar) {
             		numeros[i] = 0;
             		contar++;
             	}
			}
			
			if (contar == 0) {
				System.out.println("El n�mero no se encuentra en el array");
			} else {
				System.out.println("El array modificado queda as�: ");
				for ( i = 0; i < numeros.length; i++) {
					System.out.println(numeros[i]);
				}
			}
		}else if (numero == 4) {
			System.exit(0);
		}  
       } while (numero!=4);
	}
}
